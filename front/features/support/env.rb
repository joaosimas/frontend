require 'selenium-webdriver'
require "chromedriver-helper"
require 'capybara'
require 'capybara/cucumber'
require 'site_prism'
require 'httparty'
require "rspec"

World(Capybara::DSL)
World(Capybara::RSpecMatchers)


Capybara.configure do |config|
    config.default_driver = :selenium
    config.default_max_wait_time = 5
end


BROWSER = ENV['BROWSER']


Capybara.register_driver :selenium do |app|
    if BROWSER.eql?('chrome')
    Capybara::Selenium::Driver.new(
    app,
    :browser => :chrome,
    :desired_capabilities => Selenium::WebDriver::Remote::Capabilities.chrome(
            'chromeOptions' => {
                'args' => ['windows-size=1278,768']
            }
        )
    )
    elsif BROWSER.eql?('firefox')
    Capybara::Selenium::Driver.new(
        app,
        :browser => :firefox,
        :marionette => true
    )
    end

end