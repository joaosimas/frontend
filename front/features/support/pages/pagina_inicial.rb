class PaginaInicial < SitePrism::Page

  set_url 'https://dasa.com.br'
  element :a_empresa, '.nav-item:nth-child(1) > #navbarDropdownMenuLink'
  element :conheca_dasa, '.button-banner'
  element :linha_tempo, '.btn-content-plus'
  element :linha_tempo_decada_70,  'button[class=owl-next]'

  def  ConhecaDasa
    a_empresa.click
    conheca_dasa.click
  end

  def LinhaTempo
    linha_tempo.click
    linha_tempo_decada_70.click
  end

end

