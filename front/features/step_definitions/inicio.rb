  Dado("que eu acesse a pagina da DASA") do
    @index = PaginaInicial.new
    @index.load
    expect(current_url).to eq("https://dasa.com.br/")
end

Quando("o cliente clicar em  Conheca a Dasa") do
    @index.ConhecaDasa
    expect(page).to have_content 'REFERÊNCIA EM SAÚDE'
end

E("clicar na linha do tempo, clicar em década 60 e 70 e depois fechar") do
    @index.LinhaTempo
    expect(page).to have_content 'Década de 60'
    expect(page).to have_content 'Década de 70'
end