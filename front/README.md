# Automação do projeto FRONT

O propósito desse projeto é garantir os testes do time de Busca com foco na parte do FrontEnd, sendo assim garantindo o funcionamento do mesmo.

## Instalar RUBY
site - https://gorails.com/setup/ubuntu/16.04

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt-get update
sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev nodejs yarn

cd
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
exec $SHELL

git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
exec $SHELL

rbenv install 2.5.3
rbenv global 2.5.3
ruby -v

gem install bundler

# Configurações do projeto [Browsers]
##OBRIGATÓRIO
## Firefox - GeckoDriver
wget https://github.com/mozilla/geckodriver/releases/download/v0.23.0/geckodriver-v0.23.0-linux64.tar.gz

sudo sh -c 'tar -x geckodriver -zf geckodriver-v0.23.0-linux64.tar.gz -O > /usr/bin/geckodriver'

sudo chmod +x /usr/bin/geckodriver

rm geckodriver-v0.23.0-linux64.tar.gz

##APENAS FAZER A PARTE DO CHROME  SE O DEFAULT NÃO FUNCIONAR##
## Chrome - ChromeDriver
wget https://chromedriver.storage.googleapis.com/2.29/chromedriver_linux64.zip

unzip chromedriver_linux64.zip

sudo chmod +x chromedriver

sudo mv chromedriver /usr/bin/

rm chromedriver_linux64.zip

## Estrutura

Project
├── config
├── features
│   ├── functions
│   ├── specs
│   │   └── BDD
│   ├── step_definitions
│   │   └── Tests
│   └── suport
│   │   └── PageObjects
│   │   └── Enviroments
├── report
├── .gitignore
├── Gemfile
└── README.md

## As ferramentas

Utlizamos nessa automação:
.cucumber -- Para montar o BDD
.capybara   -- Para ser capaz de subir o browser (Chrome, Firefox).
.rspec          -- Para fazer os asserts
.*gemfile* -- Faz o import das libs que vamos utilizar

## Rodando a automação

Com o ruby instalado, execute os seguintes comandos:

bundle

O comando *bundle* baixa todas as dependências do projeto e as instala (dependências do Gemfile).

**Obs.:** É possivel que ao instalar as dependências, seja encontrado algum problema para instalar uma gem, nesse caso realize a instalação da gem especifica e prossiga com o bundle.

No diretório que for possível localizar o arquivo Gemfile, rodar o comando **cucumber**

O comando *cucumber* executa de fato todos os testes do projeto de uma unica vez.

Caso queira rodar algum teste em especifico rode o seguinte comando:
```
$ cucumber -t @tagcriada
```
**Obs.:** *tagcriada* é o nome da tag que especifica qual o teste desejado.

## Relatórios
2 relatórios serão gerados, em html e json no diretório **report**

cucumber -p chrome -p html_report -p json_report

cucumber -p firefox -p html_report -p json_report



google - jquery

<input class="gLFyf gsfi" maxlength="2048" name="q" type="text" jsaction="paste:puy29d" aria-autocomplete="both" aria-haspopup="false" autocapitalize="off" autocomplete="off" autocorrect="off" role="combobox" spellcheck="false" title="Pesquisar" value="" aria-label="Pesquisar">

$('.gLFyf');


<input class="gLFyf gsfi" maxlength="2048" name="q" type="text" jsaction="paste:puy29d" aria-autocomplete="both" aria-haspopup="false" autocapitalize="off" autocomplete="off" autocorrect="off" role="combobox" spellcheck="false" title="Pesquisar" value="" aria-label="Pesquisar">

$('input[name="q"]');